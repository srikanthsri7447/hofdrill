const data = require('./p3')


// sum of all the even position digits is odd

const oddCardNumber = data.filter((eachCard)=>{
    let evenSum = 0
    let index = 0

    for(let eachNum of (eachCard.card_number)){
        if(index % 2 ==0){
            evenSum += (Number(eachNum))
        }
        index++
    }

    if(evenSum % 2 != 0){
        return eachCard
    }
})

// console.log(oddCardNumber)

// cards that were issued before June.

const cardsBeforeJune = data.filter((eachCard)=>{
    const date = new Date(eachCard.issue_date)
    if(date.getMonth()+1 < 6){
        return eachCard
    }
})

// console.log(cardsBeforeJune);

// CVV is a random 3 digit number

const newData = data.map((eachCard)=>{
    eachCard['CVV'] = Math.floor(Math.random()*(900)+100)
    return eachCard
})

// console.log(newData)

const validData = data.map((eachCard)=>{
    eachCard.is_Valid = ''
    return eachCard
})
.map((eachCard)=>{
    const date = new Date(eachCard.issue_date)
    if(date.getMonth()+1 < 3){
        eachCard.is_Valid = 'Invalid'
    }
    else{
        eachCard.is_Valid = 'Valid'
    }
    return eachCard
})
.sort((currCard,nxtCard)=> new Date(currCard.issue_date) -  new Date(nxtCard.issue_date))
.reduce((groupData,currentValue)=>{
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const date = new Date(currentValue.issue_date)
    const month = date.getMonth()
    console.log(months[month])
    if(groupData.hasOwnProperty(months[month])){
        groupData[months[month]].push(currentValue)

    }else{
        groupData[months[month]]= []
        groupData[months[month]].push(currentValue)
        
    }
    return groupData
},{})


console.log(validData);


